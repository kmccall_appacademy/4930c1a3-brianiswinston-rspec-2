def reverser
  yield.split.map(&:reverse).join(' ')
end

def adder(num = 1)
  yield + num
end

def repeater(amount = 1)
  amount.times {yield}
end
