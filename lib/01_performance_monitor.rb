def measure(amount = 1)
  time = Time.now
  amount.times { yield }
  (Time.now - time) / amount
end
